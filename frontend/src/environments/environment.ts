// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  REST_API_PARKINGLOTS: 'http://127.0.0.1:8000/api/parkinglots',
  REST_API_HOLYDAYS: 'https://api.boostr.cl/holidays.json',
  REST_API_WEATHER: 'https://api.openweathermap.org/data/2.5/weather',
  REST_API_WEATHER_KEY: '8498ea2d256d483609f8570fec0bb9aa',
  REST_API_AUTH_LOGIN: 'http://127.0.0.1:8000/api/login',
  REST_API_AUTH_REGISTER: 'http://127.0.0.1:8000/api/register',
  REST_API_AUTH_PROFILE: 'http://127.0.0.1:8000/api/profile',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
