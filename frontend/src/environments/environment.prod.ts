export const environment = {
  production: true,
  REST_API_PARKINGLOTS: 'http://127.0.0.1:8000/api/parkinglots',
  REST_API_HOLYDAYS: 'https://api.boostr.cl/holidays.json',
  REST_API_WEATHER: 'https://api.openweathermap.org/data/2.5/weather',
  REST_API_WEATHER_KEY: '8498ea2d256d483609f8570fec0bb9aa',
  REST_API_AUTH_LOGIN: 'http://127.0.0.1:8000/api/login',
  REST_API_AUTH_REGISTER: 'http://127.0.0.1:8000/api/register',
  REST_API_AUTH_PROFILE: 'http://127.0.0.1:8000/api/profile',
};

