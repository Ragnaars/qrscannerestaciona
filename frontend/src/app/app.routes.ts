import { Routes } from '@angular/router';
import { authGuard } from 'src/app/auth/guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [authGuard],
    loadChildren: () => import('./tabs/tabs.routes').then((m) => m.routes),
  },

  {
    path: '',
    loadComponent: () => import('./auth/layouts/auth.component'),

    children: [
      {
        path: 'login',
        loadComponent: () => import('./auth/pages/login/login.component'),
      },
      {
        path: 'register',
        loadComponent: () => import('./auth/pages/register/register.component'),
      },
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      }

    ]
  }

];
