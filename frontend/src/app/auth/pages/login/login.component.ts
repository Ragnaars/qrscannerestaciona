import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { Router, RouterLink } from '@angular/router';
import { LoginResponse } from '../../models/LoginResponse.model';
import { IonContent, IonFooter, IonHeader } from '@ionic/angular/standalone';
import { HeaderComponent } from '../../components/header/header.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  standalone: true,
  imports: [ReactiveFormsModule, IonHeader, IonFooter, IonContent, HeaderComponent, FooterComponent, RouterLink]
})
export default class LoginComponent implements OnInit {
  loginForm: FormGroup;
  toastr = inject(ToastrService)
  error: any;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit() { }

  onSubmit(): void {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe(
        (response: { token: string }) => {
          this.toastr.success('Login successful');
          this.router.navigateByUrl('/tabs/tab1');
        },
        (error: HttpErrorResponse) => {
          if (error.error && error.error.message) {
            this.error = error.error.message;
          } else {
            this.error = 'An unexpected error occurred';
          }
          this.toastr.error(this.error);
        }
      );
    }
  }
}
