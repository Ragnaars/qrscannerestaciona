import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { Router, RouterLink } from '@angular/router';
import { RegisterResponse } from '../../models/RegisterResponse.model';
import { IonContent, IonFooter, IonHeader } from '@ionic/angular/standalone';
import { HeaderComponent } from '../../components/header/header.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  standalone: true,
  imports: [ReactiveFormsModule, RouterLink, IonFooter, IonHeader, IonContent, HeaderComponent, FooterComponent]
})
export default class RegisterComponent  implements OnInit {
  registerForm: FormGroup;
  toastr=inject(ToastrService)
  error:any;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.registerForm = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      passwordConfirmation: ['', Validators.required],
      phoneNumber: ['', Validators.required],
    });
  }


  ngOnInit(): void {

  }


  onSubmit(): void {
    if (this.registerForm.valid) {
      this.authService.register(this.registerForm.value).subscribe(
        (response: RegisterResponse) => {
          localStorage.setItem('token', response.token);
          this.toastr.success('Registration successful');
          this.router.navigateByUrl('/tabs/parking-lots');
        },
        (error: HttpErrorResponse) => {
          if (error.error && error.error.message) {
            this.error = error.error.message;
          } else {
            this.error = 'An unexpected error occurred';
          }
          this.toastr.error(this.error);
        }
      );
    }
  }
}

