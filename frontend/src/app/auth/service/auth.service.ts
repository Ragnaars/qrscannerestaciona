import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, tap, throwError } from 'rxjs';
import { User } from 'src/app/shared/models/User.model';
import { RegisterResponse } from '../models/RegisterResponse.model';
import { LoginResponse } from '../models/LoginResponse.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isUserLogin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.hasToken());

  public userInfo = new BehaviorSubject<any>(null);



  private REST_API_AUTH_LOGIN = environment.REST_API_AUTH_LOGIN;
  private REST_API_AUTH_REGISTER = environment.REST_API_AUTH_REGISTER;
  private REST_API_AUTH_PROFILE = environment.REST_API_AUTH_PROFILE;

  constructor(private httpClient: HttpClient, private router: Router) {
    this.monitorTokenChanges();

  }

  register(user: Partial<User>): Observable<RegisterResponse> {
    return this.httpClient.post<RegisterResponse>(this.REST_API_AUTH_REGISTER, user, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }

  login(credentials: { email: string, password: string }): Observable<{ token: string }> {
    return this.httpClient.post<{ token: string }>(this.REST_API_AUTH_LOGIN, credentials, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).pipe(
      tap(response => {
        localStorage.setItem('token', response.token);
        this.isUserLogin.next(true);
      })
    );
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  hasToken(): boolean {
    return !!this.getToken();
  }

  isLoggedIn(): boolean {
    const loggedIn = this.hasToken();
    this.isUserLogin.next(loggedIn);
    return loggedIn;
  }

  logout(): void {
    localStorage.removeItem('token');
    this.isUserLogin.next(false);
  }

  getUserInfo(): Observable<User> {
    const token = this.getToken();
    if (!token) {
      throw new Error('No token found');
    }

    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.get<User>(this.REST_API_AUTH_PROFILE, { headers });
  }

  private monitorTokenChanges() {
    window.addEventListener('storage', (event) => {
      if (event.key === 'token') {
        const token = this.getToken();
        if (!token) {
          this.isUserLogin.next(false);
          this.userInfo.next(null);
          this.router.navigate(['/login']);
        }
      }
    });
  }


}
