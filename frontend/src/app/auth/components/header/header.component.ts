import { Component, OnInit } from '@angular/core';
import { IonTitle } from '@ionic/angular/standalone';
import { IonToolbar } from '@ionic/angular/standalone';
import { IonHeader } from "@ionic/angular/standalone";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  standalone: true,
  imports: [IonHeader, IonToolbar, IonTitle ],
})
export class HeaderComponent  implements OnInit {

  constructor() { }

  ngOnInit() {}

}
