import { IonFooter, IonContent, IonRouterOutlet } from '@ionic/angular/standalone';
import { IonHeader } from '@ionic/angular/standalone';
import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from '../components/header/header.component';
import { FooterComponent } from '../components/footer/footer.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  standalone: true,
  imports: [IonHeader, IonFooter, HeaderComponent, FooterComponent, IonContent, IonRouterOutlet]
})
export default class AuthComponent  implements OnInit {

  constructor() { }

  ngOnInit() {}

}
