import { Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

export const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'parking-lots',
        loadComponent: () =>
          import('../../pages/parking-lots/parking-lots.component')
      },
      {
        path: 'profile',
        loadComponent: () =>
          import('../../pages/profile/profile.component')
      },
      {
        path: '',
        redirectTo: '/tabs/parking-lots',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/parking-lots',
    pathMatch: 'full',
  },

];
