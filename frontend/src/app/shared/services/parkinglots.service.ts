import { Injectable } from '@angular/core';
import { ParkingLot } from '../models/Parkinglot.model';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ParkinglotsService {

  private REST_API_PARKINGLOTS = environment.REST_API_PARKINGLOTS;
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  // BehaviorSubject to hold the parking lots data
  private parkingLotsSubject: BehaviorSubject<ParkingLot[]> = new BehaviorSubject<ParkingLot[]>([]);
  public parkingLots$: Observable<ParkingLot[]> = this.parkingLotsSubject.asObservable();

  constructor(private httpClient: HttpClient) {
    this.loadParkingLots(); // Load the initial data
  }

  loadParkingLots() {
    this.httpClient.get<ParkingLot[]>(this.REST_API_PARKINGLOTS, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
      .subscribe(data => {
        this.parkingLotsSubject.next(data); // Update the BehaviorSubject with new data
      });
  }

  getParkingLots(): Observable<ParkingLot[]> {
    return this.parkingLots$;
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage: string = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}. Message: ${error.message}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}

