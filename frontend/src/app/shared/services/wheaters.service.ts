import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class WheatersService {

  private REST_API_WEATHER = environment.REST_API_WEATHER;
  private REST_API_WEATHER_KEY = environment.REST_API_WEATHER_KEY;

  constructor(private httpClient: HttpClient) { }

  getWeatherByCoordinates(lat: number, lon: number): Observable<any> {
    const url = `${this.REST_API_WEATHER}?lat=${lat}&lon=${lon}&appid=${this.REST_API_WEATHER_KEY}`;
    // console.log(url);
    return this.httpClient.get(url);
  }
}
