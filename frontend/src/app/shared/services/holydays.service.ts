import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HolydaysService {

  private REST_API_HOLYDAYS = environment.REST_API_HOLYDAYS;

  constructor(private httpClient: HttpClient) { }

  getHolidays(): Observable<any> {
    return this.httpClient.get(this.REST_API_HOLYDAYS);
  }
}
