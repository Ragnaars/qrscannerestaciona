import { Component, OnInit } from '@angular/core';
import { ParkinglotsService } from '../../services/parkinglots.service';
import { ParkingLot } from '../../models/Parkinglot.model';

@Component({
  selector: 'app-card-parking-lots',
  templateUrl: './card-parking-lots.component.html',
  styleUrls: ['./card-parking-lots.component.scss'],
  standalone: true,
  imports: []
})
export class CardParkingLotsComponent implements OnInit {

  parkinglot: ParkingLot[] = [];
  totalDisponibles: number = 0;

  constructor(private parkinglotsService: ParkinglotsService) { }

  ngOnInit() {
    // Subscribe to the parking lots observable to get real-time updates

    this.parkinglotsService.getParkingLots().subscribe((res: ParkingLot[]) => {
      this.parkinglot = res.filter(item => item.available === true);
      this.totalDisponibles = this.parkinglot.length;
    });
  }
}
