import { Component, Input, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { IonContent, IonRouterOutlet, IonApp } from "@ionic/angular/standalone";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  standalone: true,
  imports: [IonApp, IonRouterOutlet, IonContent, RouterOutlet],
})
export class ContentComponent  implements OnInit {

  constructor() { }

  ngOnInit() {}

}
