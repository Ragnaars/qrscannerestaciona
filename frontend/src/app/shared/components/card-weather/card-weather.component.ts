import { Component, OnInit } from '@angular/core';
import { WheatersService } from '../../services/wheaters.service';
import { IonGrid, IonRow, IonCol } from "@ionic/angular/standalone";

const weatherDescriptionMapping: { [key: string]: string } = {
  'clear sky': 'Cielo despejado',
  'few clouds': 'Pocas nubes',
  'scattered clouds': 'Nubes dispersas',
  'broken clouds': 'Nubes rotas',
  'overcast clouds': 'Nublado',
  'shower rain': 'Aguacero',
  'rain': 'Lluvia',
  'light rain': 'Lluvia ligera',
  'moderate rain': 'Lluvia moderada',
  'heavy rain': 'Lluvia intensa',
  'thunderstorm': 'Tormenta',
  'snow': 'Nieve',
  'mist': 'Niebla',
};

@Component({
  selector: 'app-card-weather',
  templateUrl: './card-weather.component.html',
  styleUrls: ['./card-weather.component.scss'],
  standalone: true,
  imports: [IonCol, IonRow, IonGrid, ]
})
export class CardWeatherComponent  implements OnInit {

  weatherData: any;
  idIcon!: string;
  urlIcon!: string;

  constructor(private wheatersService: WheatersService) { }

  ngOnInit() {

        // Obtén los datos climáticos y asígnalos a weatherData
        this.wheatersService.getWeatherByCoordinates(-33.5922, -70.6992).subscribe(data => {
          // Convertir la temperatura de Kelvin a Celsius y redondear
          this.weatherData = data;
          // console.log(this.weatherData);
          this.weatherData.main.temp = Math.round(this.weatherData.main.temp - 273.15); // Restar 273.15, luego redondear
          this.idIcon = this.weatherData.weather[0].icon;
          this.urlIcon = 'https://openweathermap.org/img/wn/' + this.idIcon + '@2x.png'
          // console.log(this.urlIcon);


          // Obtener la descripción del clima en inglés
          const weatherDescriptionInEnglish = this.weatherData.weather[0].description;

          // Traducir la descripción al español utilizando el mapeo
          const weatherDescriptionInSpanish = weatherDescriptionMapping[weatherDescriptionInEnglish];

          // Reemplazar la descripción en inglés con la traducción en español
          this.weatherData.weather[0].description = weatherDescriptionInSpanish;


        });
  }

}
