import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { IonTitle, IonToolbar, IonGrid, IonRow, IonCol, IonButton, IonTabButton, IonIcon, IonTabs, IonTabBar } from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { carSportSharp, personSharp } from 'ionicons/icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  standalone: true,
  imports: [IonIcon, IonTabButton, IonButton, IonRow, IonGrid, IonToolbar, IonTitle, IonRow, IonCol, IonTabs, IonTabBar, RouterLink]
})
export class FooterComponent  implements OnInit {

  constructor() {
    addIcons({
      'car-sport-sharp': carSportSharp,
      'person-sharp': personSharp
    });
  }

  ngOnInit() {}

}
