import { Component, OnInit } from '@angular/core';
import { HolydaysService } from '../../services/holydays.service';
import { IonTitle } from "@ionic/angular/standalone";

@Component({
  selector: 'app-card-holydays',
  templateUrl: './card-holydays.component.html',
  styleUrls: ['./card-holydays.component.scss'],
  standalone: true,
  imports: [IonTitle, ]
})
export class CardHolydaysComponent  implements OnInit {

  constructor(private holydaysService: HolydaysService) { }
  feriados: any;
  feria2: any = [];
  feriadoMasCercano: any = {};
  ngOnInit() {
    this.holydaysService.getHolidays().subscribe((data: any) => {
      const fechaActual = new Date();

      this.feriados = data;
      this.feria2 = this.feriados.data;

      const feriadosFuturos = this.feria2.filter((feriado: any) => new Date(feriado.date) >= fechaActual);

      if (feriadosFuturos.length > 0) {
        feriadosFuturos.sort((a: any, b: any) => new Date(a.date).getTime() - new Date(b.date).getTime());
        this.feriadoMasCercano = feriadosFuturos[0];
      }


    });
  }


}
