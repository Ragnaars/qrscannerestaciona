export interface User {
  _id: string;
  _idParkingLot: string;
  fullName: string;
  email: string;
  password: string;
  phoneNumber: string;
  type: string;
  qrCode: string;
  createdAt: string;
  updatedAt: string;
}
