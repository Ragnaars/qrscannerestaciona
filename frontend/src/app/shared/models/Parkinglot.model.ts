export interface ParkingLot {
  _id: string;
  available: boolean;
  email: string;
  parkingNumeber: number;
  priority: boolean;
  type: string;
  createdAt: string;
  updatedAt: string;
}
