import mongoose from 'mongoose';

const parkinglotSchema = new mongoose.Schema({
    available: {
        type: Boolean,
        required: [true, "Please complete the field"]
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    parkingNumber: {
        type: Number,
        required: [true, "Please complete the field"],
        unique: true,
        min: [1, "Parking number must be at least 1"],
        max: [1000, "Parking number must be at most 1000"],
        trim: true
    },
    priority: {
        type: Boolean,
        required: [true, "Please complete the field"]
    },
    type: {
        type: String,
        required: [true, "Please complete the field"],
        enum: ["only", "double"]
    },
}, {
    timestamps: true,
    versionKey: false
});

const ParkingLotModel = mongoose.model('ParkingLot', parkinglotSchema);

export default ParkingLotModel;