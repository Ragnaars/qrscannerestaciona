import express from 'express';
import { getParkingLot, getParkingLots, createParkingLot, updateParkingLot, deleteParkingLot } from '../controllers/parkingLotController.js';
import { register, login, profile } from '../controllers/userController.js';
import { authenticate, authorize } from '../middlewares/authMiddleware.js';

const router = express.Router();

// Rutas de ParkingLots
router.get("/parkinglots", getParkingLots);
router.get("/parkinglots/:id", authenticate, authorize(['guard','reserver','admin']), getParkingLot);
router.post("/parkinglots", authenticate, authorize(['reserver','admin']), createParkingLot);
router.put("/parkinglots/:id", authenticate, authorize(['reserver','admin']), updateParkingLot);
router.delete("/parkinglots/:id", authenticate, authorize(['admin']), deleteParkingLot);

// Rutas de Usuarios
router.post("/register", register);
router.post("/login", login);
router.get("/profile", authenticate, profile);


export default router;